### Street Easy Scraper

The scrapper uses python's selenium to scrape the data. It uses Python 2
The following libraries are needed:
- selenium
- webdriver
- BeautifulSoup
- time
- os
- sys
- urllib
- mysql.connector
- csv
CSV is included in case data is required to be stored in a CSV file.

The libraries time, os, sys and urllib comes with default Python installation. It might be required to install other libraries using pip.

Some commands that can install required libraries are:
- $ sudo pip install selenium
- $ sudo pip install BeautifulSoup4
- $ sudo pip install mysql-connector-python-rf
- $ sudo pip install psycopg2

Run these commands only if corresponding error occours.

To run the project, simply pull the project and run command:
- python street_easy.py

You might also need to create a mysql database. I am also attaching a sql file for the database
