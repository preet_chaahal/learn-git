import selenium
from selenium import webdriver
from bs4 import BeautifulSoup
import time
import os
import sys
import urllib
import psycopg2
# from pyvirtualdisplay import Display
from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
# import mysql.connector
import csv
from re import sub
from decimal import Decimal
# cnx = psycopg2.connect(user='lkcpoeipzeyjqy', password='yhA6gh0SFtIKCWdpGx8aG_e3qN',
#                              host='ec2-54-243-48-178.compute-1.amazonaws.com',
#                              database='ddhqsmlb9l7q45')
#cursor = cnx.cursor()

reload(sys)
sys.setdefaultencoding("utf_8")
sys.getdefaultencoding()
# display = Display(visible=0, size=[800, 600])
# display.start()
firefox_capabilities = DesiredCapabilities.FIREFOX
firefox_capabilities['marionette'] = True
firefox_capabilities['binary'] = '/usr/bin/firefox'
driver = webdriver.Firefox(capabilities=firefox_capabilities)
flag=True
pagination=1

ofile  = open('streeteasy.csv', "w")
writer = csv.writer(ofile)
writer.writerow(['title','price','Details','Details Info','Listing By'])
final_list=[]
while(flag):
    driver.get('https://streeteasy.com/for-rent/nyc/price:1700-75000%7Carea:102,119,139,135?refined_search=true?page='+str(pagination))
    pagination=pagination+1
    time.sleep(5)
    page = driver.find_element_by_xpath("//*")
    allHTML=page.get_attribute('innerHTML')
    html=allHTML.encode('utf-8')

    index=html.find("We can't seem to select_one the page you're looking for.")
    if index>0:
        flag=False
    
    soup = BeautifulSoup(html, 'html.parser')
    for div in soup.select('article.item'):
        img_url=div.select_one('img')['src']
        img_url=img_url.replace("https","http")
        head=div.select_one('h3.details-title')
        title=head.select_one('a').text.strip()
        print "Title: "+str(title)
        directory = "images"
        if not os.path.exists(directory):
            os.makedirs(directory)
        image_path=directory+"/"+title+".jpg"
        try:
            urllib.urlretrieve(img_url,image_path)
        except:
            print "Image could not download"
        price=div.select_one('span.price').text.split()
        price=price[0]
        price = Decimal(sub(r'[^\d.]', '', price))
        print "Price: "+str(price)
        detail_ul=div.select_one('ul.details_info')
        details=''
        for d in detail_ul.select('li'):
            detail=d.text
            details=details+str(detail.strip())+","
        details=details.replace("'","")
        details=details.strip()
        details=details[:-1]
        print "Details:"+str(details)
        
        try:
            details_info=div.select_one('li.details_info').text.strip()
        except:
            details_info=div.select_one('li.details_info')
        details_info=details_info.replace("'","") 
        print "Details Info:"+str(details_info)
        try:
            listed_by=div.select_one('li.details_info details-info-flex').text.strip()
        except:
            listed_by=div.select_one('li.details_info details-info-flex')
        print "Listing By:"+str(listed_by)

        

        t_details = details.split(',')
        baths = 0
        size = ''
        for feature in t_details:
            if 'bath' in feature:
                baths = int(filter(str.isdigit, feature))
            if 'ft' in feature:
                size = feature
        print "Baths: "+str(baths)


        # sql="Select id from entries where address='"+str(title)+"' and price='"+str(price)+"' and misc='"+str(details)+"' and image_link='"+str(image_path)+"'  and additional_features='"+str(details_info)+"' and landlord='"+str(listed_by)+"' and bathrooms='"+str(baths)+"' and size='"+str(size)+"'"
        # print sql
        # cursor.execute(sql)
        # row_count_details = cursor.rowcount
        # cnx.commit()
        # if row_count_details==0:
        # sql="insert into entries(address,price,misc,image_link,additional_features,landlord,bathrooms,size)VALUES('"+str(title)+"','"+str(price)+"','"+str(details)+"','"+str(image_path)+"','"+str(details_info)+"','"+str(listed_by)+"','"+str(baths)+"','"+str(size)+"')"
        # print "Inserting query: "+sql
        # cursor.execute(sql)
        # cnx.commit()
        print("Writing CSV data"+ [str(title),str(price),str(details),str(details_info),str(listed_by),str(image_path)])
        writer.writerow([str(title),str(price),str(details),str(details_info),str(listed_by),str(image_path)])

driver.close()
# ifile.close()

print("End of script")