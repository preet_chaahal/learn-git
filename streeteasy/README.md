### Street Easy Ad Post To Renthop, Apartments and NakedApartments

### The scrapper uses python's selenium to scrape the data. It uses Python 2.7
### The following libraries are needed:
- selenium
- webdriver
- BeautifulSoup
- time
- os
- sys
- urllib
- mysql.connector
- csv
- re
- random
- decimal
- requests

### Two CSV Includes.
- streeteasy_urls.csv ( Read all listing urls from street_easy.com website)
- streeteasy_data.csv ( Read all info of listing from street_easy.com website)

### The libraries time, os, sys and urllib comes with default Python installation. It might be required to install other libraries using pip.

### Some commands that can install required libraries are:
- $ sudo pip install selenium
- $ sudo pip install BeautifulSoup4

### If any module is not installed then following command use for module
- $ sudo pip install MODULE_NAME

Run these commands only if corresponding error occours.

### To run the project, simply pull the project and run command:

### Read all listing urls from https://streeteasy.com/
- python street_easy_urls.py

### Read all listing info from https://streeteasy.com/
- python street_easy_data.py

### Post Ad to https://www.renthop.com
- python renthop.py

### Post Ad to http://nakedapartments.com
- python nakedapartments.py

### Post Ad to http://aparments.com
- python apartments.py

