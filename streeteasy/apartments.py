import selenium
from selenium import webdriver
from selenium.webdriver.firefox.firefox_profile import FirefoxProfile
from selenium.webdriver.chrome.options import Options
from bs4 import BeautifulSoup
import os, sys, csv, re, random
from selenium.webdriver.common.by import By
import time
import requests
from selenium.webdriver.common.keys import Keys
import decimal
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import Select
from decimal import Decimal
from helper_functions import(
	login_user, open_url,
	form_select_radio, form_select_option,
	form_input_text, form_select_checkbox,
	form_upload_file, form_button_click,
	display_notice,
)

"""
Defining variables
"""
URL_HOME = "https://www.apartments.com/"
URL_ADD_EDIT_LISTING = "https://www.apartments.com/add-edit-listing/"
LISTING = "https://www.apartments.com/customers/manage-my-listings/?page=1&items=25"
LOGIN_USERNAME = "gurpreet.24.chahal@gmail.com"
LOGIN_PASSWORD = "QazWsxEdc123"
STREET_EASY_CSV_DATA = "streeteasy_data.csv"
CONTACT_FIRST_NAME = "Alex"
CONTACT_LAST_NAME = "Prose"
#CONTACT_PHONE_NUMBER = "917 242 2653"
CONTACT_PHONE_NUMBER = "347 815 5542"
CONTACT_EMAIL = "info@sq-develop.tech"

print("Starting up the Python Automation Tool..")

"""
Setting things up with web-driver
"""
## Initialize Firefox browser
browser = webdriver.Firefox()

## Maximize window and open the base url
#browser.maximize_window()
# options = webdriver.ChromeOptions()
# options.add_argument("--disable-notifications")
# options.add_argument("--disable-popup-blocking")
# options.add_argument("--start-maximized")
# options.add_argument("-incognito");
# options.add_argument("--no-sandbox");
# browser = webdriver.Chrome(chrome_options=options)
#browser = webdriver.Chrome()

## Step 1
# 1.1 Login to the site will be one time thing
login_user(browser, URL_HOME, LOGIN_USERNAME, LOGIN_PASSWORD)

"""
	Static Form Values
"""
f2 = open(STREET_EASY_CSV_DATA)
csv_items = csv.DictReader(f2)
renthop_data = []
for i, row in enumerate(csv_items):
	renthop_data.append(row)
 
j=0       
if renthop_data:    
	for data in renthop_data:
		j += 1
		"""
		form = {
			'search_by_address': '27 West 27th Street, South Chicago Heights, IL',
			'location_unit_number': '12',
			'listing_type': 'Apartment',
			'beds': '3.0',
			'baths': '3.5',
			'square_feet': '2',
			'rent': '420',
			'deposit': '0',
			'available_date': '5/24/2018',
			'lease_length': '12',
			'photos': [
				"/home/preet/Pictures/FRONT-400x220.jpg",
			],
			'photo_descriptions': [
				"First uploaded file, pretty admirable place",
			],
			'contact': {
				'first_name': 'John',
				'last_name': 'Doe',
				'email': 'gurpreet.24.chahal@gmail.com',
				'phone': '2051231312',
				'contact_preference': 'Phone & Email',
				'hide_my_name': True
			},
			'description': 'A 5-star place to be.',
			'amenities': ['dogOk', 'catOk', 'furnished', 'noSmoking', 'wheelchair'],
			'laundry_type': 'Laundry Facilities',
			'parking_type': 'Covered',
			'parking_fee': '12',
		}
		"""
		
		photos = data["images"]
		photosArray = photos.split(",")
		
		amenities = data["amenities"]
		building_amenities = data["building_amenities"]
		listing_amenities = data["listing_amenities"]
		all_amenities = amenities + ',' + building_amenities
		all_amenities = all_amenities.replace(' ',',').lower()
		amenities_array = re.split(',',all_amenities)
		checkboxArray = amenities_array
		
		if data["description"]:
			description = data["description"] + "\n\n" + amenities + "\n" + building_amenities
		else:
			description = amenities + "\n" + building_amenities
		
		address_array = data["address"].split('#', 1)
		address = address_array[0]
		location_unit_number = address_array[1]
		
		state = data["state"]
		state = ''.join(i for i in state if not i.isdigit())
		if state:
			search_by_address = address+','+state
		else:
			search_by_address = address
		
		random_variables = ['Elegant','Magnifcient','Massive','Gigantic','Big','Beefy','Stellar','Stunning','Opulent','Solid','Stupendous','Tremendous','Goliath','Bright']
		apartment_type = random_variables[random.randrange(len(random_variables))]
		description_template  = "%s %s bedroom apartment with big queen sized + bedrooms, a large kitchen/common area, and big closets. Located on a prime block in %s"  %(apartment_type, str(data["beds"]),data["rental_unit"])  
		description_template += "\n"+'Live better. Call Alex today to set up your showing: 917-242-2653'
		description = description_template
		#sys.exit(1)
		form = {
			'search_by_address': search_by_address,
			'location_unit_number': location_unit_number,
			'listing_type': 'Apartment',
			'beds': format(float(data["beds"]), '.1f'),
			'baths': format(float(data["baths"]), '.1f'),
			'square_feet': data["sq_ft"] if data["sq_ft"] != "0" else "1",
			'rent': data["price"],
			'deposit': '0',
			'available_date': '5/24/2018',
			'lease_length': '12',
			'photos': photosArray,
			'photo_descriptions': [
				"First uploaded file, pretty admirable place",
			],
			'contact': {
				'first_name': CONTACT_FIRST_NAME,
				'last_name': CONTACT_LAST_NAME,
				'email': CONTACT_EMAIL,
				'phone': CONTACT_PHONE_NUMBER,
				'contact_preference': 'Phone & Email',
				'hide_my_name': True
			},
			'description': description,
			'amenities': ['noPets', 'furnished', 'noSmoking', 'wheelchair'],
			'laundry_type': 'Laundry Facilities',
			'parking_type': 'Covered',
			'parking_fee': '0',
		}
		
		## Step 2
		# 2.1 Open listing page
		open_url(browser, URL_ADD_EDIT_LISTING)
		time.sleep(3)
		
			
		
		print("Automating form entry...")
		print('##########################################################################################')
		print('Add Listing: '+ str(j))
		

		# Details
		## Search by address 
		
		try:
			display_notice('form', 'Setting Search by address: '+form['search_by_address'])
			browser.find_element_by_id("locationAddressLookup").send_keys(form['search_by_address'] + Keys.ENTER)
			time.sleep(4)
		except Exception as e:
			print('Reload Page');
			WebDriverWait(browser, 5).until(EC.alert_is_present())
			alert = browser.switch_to_alert()
			alert.accept()
			time.sleep(4)
			display_notice('form', 'Setting Search by address: '+form['search_by_address'])
			browser.find_element_by_id("locationAddressLookup").send_keys(form['search_by_address'] + Keys.ENTER)
		
		
		try:
			hidden_element = browser.find_element(By.XPATH, "//div[@class='btnGroup']//button[text()='Okay']").click()
			print("Popup element found")
		except Exception as e:
			display_notice('form', 'Setting Square Feet: '+form['location_unit_number'])
			form_input_text(browser, '#locationUnitNumber', form['location_unit_number'], True)

			# 2.2 Process add listing form
			# 2.2.1 Select listing type (radio)
			display_notice('form', 'Setting Listing type: '+form['listing_type'])
			form_select_radio(browser, '#listingTypeRadios', form['listing_type'])
			browser.find_element(By.XPATH, "//form[@id='listingTypeRadios']//div[@class='radioGroup']//span[contains(text(),"+form['listing_type']+")]").click()

			# 2.2.2 Select beds (select)
			display_notice('form', 'Setting Beds: '+form['beds'])
			form_select_option(browser, '.bootstrap-select.beds', form['beds'])

			# 2.2.3 Select baths (select)
			display_notice('form', 'Setting Baths: '+form['baths'])
			form_select_option(browser, '.bootstrap-select.baths', form['baths'])

			# 2.2.4 Enter square feets (text)
			display_notice('form', 'Setting Square Feet: '+form['square_feet'])
			form_input_text(browser, 'sf-clone', Keys.DELETE+Keys.DELETE+form['square_feet'])

			# 2.2.5 Enter rent (text)
			display_notice('form', 'Setting Rent: '+form['rent'])
			form_input_text(browser, 'rent-clone', form['rent'])

			# 2.2.6 Enter deposit (text)
			display_notice('form', 'Setting Deposit: '+form['deposit'])
			form_input_text(browser, 'deposit-clone', form['deposit'])

			# 2.2.7 Enter available date (text)
			display_notice('form', 'Setting Available Date: '+form['available_date'])
			form_input_text(browser, '#datepicker', form['available_date'], True)

			# 2.2.8 Enter lease length (text)
			display_notice('form', 'Setting Lease Length: '+form['lease_length'])
			form_input_text(browser, 'leaselength-clone', Keys.DELETE+Keys.DELETE+Keys.BACKSPACE+Keys.BACKSPACE+form['lease_length'])

			# 2.2.8 Photos
			time.sleep(1)
			# Uploading photos
			file_input_xpath_selector = "//div[@class='photosWrapper']//input[@type='file']"

			i = 0
			#print(form['photos'])
			for photo in form['photos']:
				if(i < 12):	
					folder = os.getcwd()+"/"
					photo_path = folder + photo
					print(photo_path)
					display_notice('form', 'Uploading photo: '+ photo)
					form_upload_file(browser, file_input_xpath_selector, photo_path)
					time.sleep(2)
					i+=1

			# 2.2.9 Fill contact details
			print("Contact details")

			display_notice('form', 'Firstname: '+form['contact']['first_name'])
			form_input_text(browser, "firstname", form['contact']['first_name'])

			display_notice('form', 'Lastname: '+form['contact']['last_name'])
			form_input_text(browser, "lastname", form['contact']['last_name'])

			display_notice('form', 'Email: '+form['contact']['email'])
			form_input_text(browser, "email", form['contact']['email'])

			display_notice('form', 'Phone: '+form['contact']['phone'])
			form_input_text(browser, "phone-clone", form['contact']['phone'])

			# 2.2.2 Select beds (select)
			display_notice('form', 'Contact preference: '+form['contact']['contact_preference'])
			form_select_option(browser, '.bootstrap-select.contactpreference', form['contact']['contact_preference'])

			val = form['contact']['hide_my_name']
			if val:
				msg = "Hide my name from apartments"
			else:
				msg = "Don't hide name from apartments"
			display_notice('form', msg)
			form_select_checkbox(browser, 'hideContact', val)


			"""
				Description & Amenities
			"""
			print("Setting up Description & Amenities")
			display_notice('form', 'Description: '+form['description'])
			form_input_text(browser, "description", form['description'])

			print("Processing amenities...")
			for amenity in form['amenities']:
				display_notice('form', amenity+': '+ "True")
				form_select_checkbox(browser, amenity, True)

			display_notice('form', 'Laundry Type: '+form['laundry_type'])
			form_select_option(browser, '.bootstrap-select.laundry', form['laundry_type'])

			display_notice('form', 'Parking Type: '+form['parking_type'])
			form_select_option(browser, '.bootstrap-select.parking', form['parking_type'])

			display_notice('form', 'Parking Fee: '+form['parking_fee'])
			form_input_text(browser, "#parkingFee-clone", form['parking_fee'], True)

			# Terms & conditions
			display_notice('form', 'Terms and Conditions accepted')
			form_select_checkbox(browser, "agreeToTerms", True)


			## Working with captcha
			time.sleep(2)
			print("Trying captcha automatically")
			iframe = browser.find_elements_by_tag_name('iframe')[0]
			browser.switch_to_frame(iframe)

			print("Processing re-captcha...")
			browser.find_element_by_id("recaptcha-anchor").click()
			browser.switch_to_default_content()
			## Captcha processed

			time.sleep(5)
			print("Trying to Submit form")
			# form_button_click(browser, "#headerSubmit")
			browser.find_element_by_id('headerSubmit').send_keys(Keys.ENTER)
			time.sleep(2)
			try:
				hidden_element = browser.find_element(By.XPATH, "//div[@class='btnGroup']//button[text()='Okay']").click()
			except Exception as e:
			        print('Captcha invalid.So ad listing not saved.')

print("End of execution, exiting the tool...")
browser.close()