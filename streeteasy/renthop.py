﻿# -*- coding: utf-8 -*-
#=============================================================================
#title           :rentop.py
#description     :This automation script for. upload images on redbubble.com
#author          :Rajinder Sharma
#date            :21-03-2018
#version         :1.0
#usage           :rentop.py
#notes           :
#python_version  :2.7  
#=============================================================================
import requests
from selenium.webdriver.chrome.options import Options
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys
import time, os, json, threading
from selenium.webdriver.support.ui import Select
from collections import OrderedDict
import sys
import csv
import re, random
from decimal import Decimal
LISTING_URL = "https://www.renthop.com/r/listings/post"
CONTACT_NAME = "Alex Prose"
#CONTACT_PHONE_NUMBER = "917 242 2653"
CONTACT_PHONE_NUMBER = "347 815 5542"
CONTACT_EMAIL = "info@sq-develop.tech"
#LOGIN_USERNAME = "sq_rajinder@gmail.com"
#LOGIN_PASSWORD = "123456789"

LOGIN_USERNAME = "Aprose@citihabitats.com"
LOGIN_PASSWORD = "ALPO@387"

from helper_functions import(
	login_user, open_url,
	form_select_radio, form_select_option,
	form_input_text, form_select_checkbox,
	form_upload_file, form_button_click,
	display_notice, write_chunk_to_file,
	read_as_soup, write_to_file,
	get_full_img_path
)

def get_driver():
    #options = Options()
    options = webdriver.ChromeOptions()
    options.add_argument("--disable-notifications")
    options.add_argument("--disable-popup-blocking")
    options.add_argument("--start-maximized")
    options.add_argument("-incognito");
    driver = webdriver.Chrome(chrome_options=options)
    return driver

def handle_dialog(element_initiating_dialog, dialog_text_input, driver):
    def _handle_dialog(_element_initiating_dialog):
        _element_initiating_dialog.click() # thread hangs here until upload dialog closes
    t = threading.Thread(target=_handle_dialog, args=[element_initiating_dialog] )
    t.start()
    time.sleep(1) # poor thread synchronization, but good enough

    upload_dialog = driver.switch_to_active_element()
    upload_dialog.send_keys(dialog_text_input)
    upload_dialog.send_keys(Keys.ENTER)

def image_upload(url):
    driver = get_driver()
    driver.get(url)
    time.sleep(2)
    #open sign in popup and fill the email and password
    driver.find_element_by_xpath("//div[@class='d-none d-md-block float-right']//a[text()='Post Rental']").click()
    email_edit = driver.find_element_by_xpath('//*[@id="login_form"]/div[2]/input[2]')
    
    time.sleep(1)
    pw_edit2 = driver.find_element_by_xpath('//*[@id="login_password"]')
    time.sleep(1)
    pw_edit1 = driver.find_element_by_xpath('//*[@id="password-clear"]')
    login_button = driver.find_element_by_xpath('//*[@id="login_form"]/div[5]/div[1]/button')
    
    if email_edit and pw_edit1 and login_button:
        email_edit.click()
        email_edit.send_keys(LOGIN_USERNAME)
        time.sleep(1)
        pw_edit1.click()
        time.sleep(1)
        pw_edit2.click()
        time.sleep(5)
        driver.execute_script("arguments[0].click();", pw_edit2)
        pw_edit2.send_keys(LOGIN_PASSWORD)
        time.sleep(1)
        login_button.click()
    
    time.sleep(2)
    
    f2 = open('streeteasy_data.csv')
    csv_items = csv.DictReader(f2)
    renthop_data = []
    for i, row in enumerate(csv_items):
        renthop_data.append(row)
        
    
    for data in renthop_data:
        if data["address"]:
            # First form
            """
            try:
                im_select = Select(driver.find_element_by_xpath('//*[@id="purpose_type"]'))
                im_select.select_by_visible_text(data["purpose_type"])
                time.sleep(1)
            except:
                print( "purpose_type field error")
            
            try:
                agent_input = driver.find_element_by_xpath('//*[@id="purpose-agent"]/input')
                agent_input.click()
                agent_input.send_keys(data["additional-details-agent"])
                time.sleep(2)
            except:
                print( "additional-details-agent field error")
            
            try:
                looking_select = Select(driver.find_element_by_xpath('//*[@id="listing_type"]'))
                looking_select.select_by_visible_text(data["listing_type"])
                time.sleep(1)
            except:
                print( "listing_type field error")
            """
            """
            try:
                phone_input = driver.find_element_by_xpath('/html/body/div[4]/div/div/div/div[2]/div[1]/form/div/input')
                phone_input.click()
                phone_input.send_keys(data["phone_number1"])
                time.sleep(1)
            except:
                print( "phone_number1 field error")
            """
            """
            try:
                agent_contact_select = Select(driver.find_element_by_xpath('//*[@id="agent_contact"]'))
                agent_contact_select.select_by_visible_text(data["agent_contact"])
                time.sleep(1)
            except:
                print( "agent_contact field error")
            
            try:
                note_input = driver.find_element_by_xpath('//*[@id="note"]')
                note_input.click()
                note_input.send_keys(data["note"])
                time.sleep(1)
            except:
                print( "note field error")
            
            try:
                next_button = driver.find_element_by_xpath('/html/body/div[4]/div/div/div/div[2]/div[1]/form/div/div[14]/button')
                next_button.click()
            except:
                print( "button field error") 
            
            """
            time.sleep(2)
            
            #second form
            address_array = data["address"].split('#', 1)
            address = address_array[0]
            unit_number = address_array[1]
            try:
                street_address_input = driver.find_element_by_xpath('//*[@id="street_address"]')
                street_address_input.click()
                street_address_input.clear();
                street_address_input.send_keys(address)
                time.sleep(1)
            except:
                print("Address is empty")
                
            try:
                if data["state"]:
                    citystate_input = driver.find_element_by_xpath('//*[@id="citystate"]')
                    citystate_input.click()
                    citystate_input.clear();
                    citystate_input.send_keys(data["state"])
                    time.sleep(1)
            except:
                print("state is empty")
            
            try:
                display_address_input = driver.find_element_by_xpath('//*[@id="display_address"]')
                display_address_input.click()
                display_address_input.clear();
                display_address_input.send_keys(address)
                time.sleep(1)
            except:
                print("address2 is empty")
            
            try:
                beds = int(round(float(data["beds"]))) if type(data["beds"]) == float else int(data["beds"])
                if beds > 8:
                    beds = 8
                bedrooms_select = Select(driver.find_element_by_xpath('//*[@id="bedrooms"]'))
                bedrooms_select.select_by_value(str(beds))
                time.sleep(1)
            except:
                print("bedrooms is empty")
            try:
                baths = int(round(float(data["baths"]))) if type(data["baths"]) == float else int(data["baths"])
                if int(baths) > 5:
                    baths = 5
                bathrooms_select = Select(driver.find_element_by_xpath('//*[@id="bathrooms"]'))
                bathrooms_select.select_by_value(str(baths))
                time.sleep(1)
            except:
                print("bathrooms is empty")
            #time.sleep(50)
            #sys.exit(1)
            try:
                price_input = driver.find_element_by_xpath('//*[@id="price"]')
                price_input.click()
                price_input.clear();
                price_input.send_keys(data["price"])
                time.sleep(1)
            except:
                print("price value is empty")
            
            try:
                unit_input = driver.find_element_by_xpath('//*[@id="unit"]')
                unit_input.click()
                unit_input.clear();
                unit_input.send_keys(unit_number)
                time.sleep(1)
            except:
                print("unit value is empty")
            
            try:
                square_input = driver.find_element_by_xpath('//*[@id="square_feet"]')
                square_input.click()
                square_input.clear();
                square_input.send_keys(data["sq_ft"])
                time.sleep(1)
            except:
                print("square_feet value is empty")
            
            try:
                if data["availability_date"]:
                    date_input = driver.find_element_by_xpath('//*[@id="availability_date"]')
                    date_input.click()
                    date_input.clear();
                    time.sleep(1)
                    date_input.send_keys(data["availability_date"])
                    time.sleep(1)
            except:
                print("availability_date value is empty")
            
            try:
                random_variables = ['Elegant','Magnifcient','Massive','Gigantic','Big','Beefy','Stellar','Stunning','Opulent','Solid','Stupendous','Tremendous','Goliath','Bright']
                apartment_type = random_variables[random.randrange(len(random_variables))]
                description_template  = "%s %s bedroom apartment with big queen sized + bedrooms, a large kitchen/common area, and big closets. Located on a prime block in %s"  %(apartment_type, str(data["beds"]),data["rental_unit"])  
                description_template += "\n"+'Live better. Call Alex today to set up your showing: 917-242-2653'
                description = description_template
                
                desc_input = driver.find_element_by_xpath('//*[@id="description"]')
                desc_input.click()
                desc_input.clear();
                desc_input.send_keys(description)
                time.sleep(5)
            except:
                print("description value is empty")
                
            try:
                amenities = data["amenities"]
                building_amenities = data["building_amenities"]
                listing_amenities = data["listing_amenities"]
                all_amenities = amenities + ',' + building_amenities
                all_amenities = all_amenities.replace(' ',',').lower()
                amenities_array = re.split(',',all_amenities)
                checkboxArray = amenities_array
                print(checkboxArray)
                #print(checkboxArray);
                try:
                    #script = "jQuery(document).ready(function(){ var checkboxArray = %s,i = 0; jQuery('input[type=checkbox]').each(function(){ var chk_value = jQuery(this).val().toLowerCase();; data_value = checkboxArray[i];if(jQuery.inArray(chk_value, checkboxArray) > -1) { jQuery(this).prop('checked', true); } i++;});});" % checkboxArray
                    script = "function common(arr1, arr2) {var newArr = []; newArr = arr1.filter(function(v){ return arr2.indexOf(v) >= 0;}); newArr.concat(arr2.filter(function(v){ return newArr.indexOf(v) >= 0;})); return newArr; }; jQuery(document).ready(function() { var checkboxArray = %s; jQuery('input[type=checkbox]').each(function() { var chk_value = jQuery(this).val().toLowerCase();var checkboxId = jQuery(this).attr('id');checkboxValueArray = chk_value.split(' ');var matchArray = common(checkboxValueArray, checkboxArray);if (matchArray && matchArray.length != 0) { jQuery('#'+checkboxId).prop('checked', true); }});});" % checkboxArray
                    driver.execute_script(script) 
                except:
                    print("jquery value is empty")
            except:
                print( "Checkbox selection error")
            
            #sys.exit(1)
            time.sleep(2)
            try:
                building_amenities = data["building_amenities"]
                building_amenities = ",".join(OrderedDict.fromkeys(building_amenities.split(',')))
                extra_input = driver.find_element_by_xpath('//*[@id="extra_features"]')
                extra_input.click()
                extra_input.clear();
                extra_input.send_keys(building_amenities)
                time.sleep(1)
            except:
                print( "extra_features value is empty")
            
            try:
                contact_input = driver.find_element_by_xpath('//*[@id="contact_name"]')
                contact_input.click()
                contact_input.clear();
                contact_input.send_keys(CONTACT_NAME)
                time.sleep(1)
            except:
                print( " contact_name value is empty")
            
            try:
                phone2_input = driver.find_element_by_xpath('//*[@id="phone_number"]')
                phone2_input.click()
                phone2_input.clear();
                phone2_input.send_keys(CONTACT_PHONE_NUMBER)
                time.sleep(1)
            except:
                print( "phone_number value is empty")
            
            try:
                email_input = driver.find_element_by_xpath('//*[@id="email"]')
                email_input.click()
                email_input.clear();
                email_input.send_keys(CONTACT_EMAIL)
                time.sleep(1)
            except:
                print( "email value is empty")
            
            try:
                website_input = driver.find_element_by_xpath('//*[@id="website"]')
                website_input.click()
                website_input.clear();
                website_input.send_keys(data["website"])
                time.sleep(1)
            except:
                print( "website value is empty")
            
            time.sleep(1)
            #multiple images uploading
            try:
                if data["images"]:
                    images = data["images"]
                    imagesArray = images.split(",")
                    k = 0;
                    for image in imagesArray:
                        if(k < 12):
                            #print(image)
                            folder = os.getcwd()+"/"
                            image_path = folder + image
                            try:
                                WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH, "//input[@type='file']"))).send_keys(image_path)
                                time.sleep(15)
                                finish_link = driver.find_element_by_xpath('//*[@id="finish_link"]')
                                finish_link.click()
                                time.sleep(2)
                                k += 1 
                            except:
                                print( "image upload error")
                            
                    time.sleep(2)
            except:
                print( "images value is empty")
            try:
                submit_form_button = driver.find_element_by_xpath('//*[@class="font-size-10 bg-white font-blue bold"]')
                submit_form_button.click()
                time.sleep(5)
            except:
                print("submit button click error")
            try:
                continue_button = driver.find_element_by_xpath('/html/body/div[4]/div/div/div/div[2]/div/a')
                continue_button.click() 
            except:
                print("RentHop credits 0");
            time.sleep(5)
            open_url(driver, LISTING_URL)
            time.sleep(5)
    driver.close()
    print("End of script")        

site_urls = ['https://www.renthop.com/']
for url in site_urls:
    image_upload(url)

