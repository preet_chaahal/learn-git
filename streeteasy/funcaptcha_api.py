import time
import requests
from python3_anticaptcha import FunCaptchaTask

class FuncaptchaAPI:
	ANTICAPTCHA_KEY = "ff687bda7bc0069cf0ebdf984f5bed82" 

	# G-ReCaptcha site key. Website google key. 
	SITE_KEY  =  '6Lf93goUAAAAAJKhC4y-Ok88s72iUJ8UX4bLQMmw' 

	# Link to a page with a cap. Page url. 
	PAGE_URL  =  'https://chaturbate.com/accounts/register/' 

	r = None

	user_answer = None

	def __init__(self, site_key=None, page_url=None):
		# self.set_anticaptcha_key = anticaptcha_key
		if not site_key:
			site_key = self.SITE_KEY
		self.set_site_key(site_key)
		self.process_reptcha_request()

	def get_anticaptcha_key(self):
		return self.ANTICAPTCHA_KEY

	def get_site_key(self):
		return self.SITE_KEY

	def get_page_url(self):
		return self.PAGE_URL

	def set_anticaptcha_key(self, key=None):
		if key == None:
			self.ANTICAPTCHA_KEY = key

	def set_site_key(self, key=None):
		self.SITE_KEY = key

	def set_page_url(self, url=None):
		if url == None:
			self.PAGE_URL = url

	def process_reptcha_request(self):
		repeat = False
		# form_data = {"clientKey": self.ANTICAPTCHA_KEY, "task": {"type": "FunCaptchaTaskProxyless", "websiteURL": "https://www.nakedapartments.com/", "websitePublicKey": self.SITE_KEY}}
		# r = requests.post("http://api.anti-captcha.com/createTask", data=form_data)
		# self.r = r
		self.user_answer = FunCaptchaTask.FunCaptchaTask(anticaptcha_key = self.ANTICAPTCHA_KEY, poxyType="http", proxyAddress="8.8.8.8", proxyPort="8080").captcha_handler(websiteURL=self.PAGE_URL, websitePublicKey=self.SITE_KEY)
		print("Website wublic key=")
		print(self.SITE_KEY)
		# while not repeat:
		# 	try:
		# 		self.user_answer = FunCaptchaTask.FunCaptchaTask(anticaptcha_key = self.ANTICAPTCHA_KEY).captcha_handler(websiteURL=self.PAGE_URL, websitePublicKey=self.SITE_KEY)
		# 		repeat = True
		# 	except:
		# 		print("Error connecting re-captcha service.. Retrying")
		# 		time.sleep(1)
		# 		repeat = False
		# self.user_answer = r.text
		return self.user_answer

	def get_response(self):
		print("Requesting response in "+ str(30) + " seconds...")
		time.sleep(30)
		self.process_reptcha_request()
		# self.r
		print("Response received from API")
		print(self.user_answer)
		return self.user_answer