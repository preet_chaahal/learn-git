import selenium
from selenium import webdriver
from selenium.webdriver.firefox.firefox_profile import FirefoxProfile
from selenium.webdriver.chrome.options import Options
from bs4 import BeautifulSoup
import os, sys, csv, re, random
from selenium.webdriver.common.by import By
import time
import requests
from selenium.webdriver.common.keys import Keys
import decimal
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import Select
from decimal import Decimal
from time import gmtime, strftime
from helper_functions import(
	login_user_nakedapartments, open_url,
	form_select_radio, form_select_option,
	form_input_text, form_select_checkbox,
	form_upload_file, form_button_click,
	display_notice,form_select_checkbox2,
)
from funcaptcha_api import *

"""
Defining variables
"""
URL_HOME_HTTP = "http://www.nakedapartments.com/"
URL_HOME_HTTPS = "https://www.nakedapartments.com/"
#URL_LISTING = "http://www.nakedapartments.com/broker/listings"
URL_ADD_EDIT_LISTING = "https://www.nakedapartments.com/broker/listings/new"
URL_ADD_EDIT_LISTING2 = "http://www.nakedapartments.com/broker/listings/new"
#URL_ADD_EDIT_LISTING = "http://localhost/test/naked.html"
LISTING = "https://www.apartments.com/customers/manage-my-listings/?page=1&items=25"

# LOGIN_USERNAME = "Aprose@citihabitats.com"
# LOGIN_PASSWORD = "ALPO@387"
LOGIN_USERNAME = "gurpreet.24.chahal@gmail.com"
LOGIN_PASSWORD = "qazwsxedc123"

STREET_EASY_CSV_DATA = "streeteasy_data.csv"
CONTACT_FIRST_NAME = "Alex"
CONTACT_LAST_NAME = "Prose"
#CONTACT_PHONE_NUMBER = "917 242 2653"
CONTACT_PHONE_NUMBER = "347 815 5542"
CONTACT_EMAIL = "info@sq-develop.tech"

print("Starting up the Python Automation Tool..")

"""
Setting things up with web-driver
"""
## Initialize Firefox browser
# browser = webdriver.Firefox()

# Maximize window and open the base url
# browser.maximize_window()
options = webdriver.ChromeOptions()
options.add_argument("--disable-notifications")
options.add_argument("--disable-popup-blocking")
options.add_argument("--start-maximized")
options.add_argument("--no-sandbox")
#options.add_argument("-incognito");
browser = webdriver.Chrome(chrome_options=options)

## Step 1
# 1.1 Login to the site will be one time thing
login_user_nakedapartments(browser, URL_HOME_HTTP, URL_HOME_HTTPS, LOGIN_USERNAME, LOGIN_PASSWORD)

"""
	Static Form Values
"""
f2 = open(STREET_EASY_CSV_DATA)
csv_items = csv.DictReader(f2)
renthop_data = []
for i, row in enumerate(csv_items):
	renthop_data.append(row)
        
if renthop_data:    
	for data in renthop_data:
		## Step 2
		# 2.1 Open listing page
		try:
			open_url(browser, URL_ADD_EDIT_LISTING)
		except:
			f_captcha_api = FuncaptchaAPI()
			funcaptcha_form_action = browser.find_element_by_xpath("//form[@id=\'distilCaptchaForm\']").get_attribute('action') 
			funcaptcha_form_action_params = funcaptcha_form_action.split('requestId=')[1]
			funcaptcha_token = funcaptcha_form_action_params.split('&')[0]
			print("Fun captcha token for site is")
			print(funcaptcha_token)
			print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
		time.sleep(1)
		repeat = False
		while True:
			try:
				browser.find_element_by_xpath('//h2[text()="Oh no!"]')
				#print('Captcha founded. Please fill up captcha')
				repeat = True
				open_url(browser, URL_ADD_EDIT_LISTING2)
			except:
				try:
					browser.find_element_by_xpath('//h2[text()="Pardon Our Interruption"]')
					#print('Captcha founded. Please fill up captcha')
					repeat = True
				except:
					print("Captcha break")
					## Attempt funcaptcha solving in 2 steps
					# Step 1: Find site public key
					print("Fun captcha token for site is")
					print(funcaptcha_token)
					print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
					# f_captcha_api.set_website_public_key('token-here') 
					# Step 2: Request Funcaptcha solve service

					repeat = False
					break;
		
		photos = data["images"]
		photosArray = photos.split(",")
		
		amenities = data["amenities"]
		building_amenities = data["building_amenities"]
		listing_amenities = data["listing_amenities"]
		all_amenities = amenities + ',' + building_amenities
		all_amenities = all_amenities.replace(' ',',').lower()
		amenities_array = re.split(',',all_amenities)
		checkboxArray = amenities_array
		
		if data["description"]:
				description = data["description"] + "\n\n" + amenities + "\n" + building_amenities
		else:
				description = amenities + "\n" + building_amenities
		
		address_array = data["address"].split('#', 1)
		address = address_array[0]
		location_unit_number = address_array[1]
		
		state = data["state"]
		zipcode = re.match('^.*(?P<zipcode>\d{5}).*$', state)
		if zipcode:
			zipcode = zipcode.groupdict()['zipcode']
		else:
		        zipcode = ""
		state = ''.join(i for i in state if not i.isdigit())
		if state:
			search_by_address = address+','+state
		else:
			search_by_address = address
		
		random_variables = ['Elegant','Magnifcient','Massive','Gigantic','Big','Beefy','Stellar','Stunning','Opulent','Solid','Stupendous','Tremendous','Goliath','Bright']
		apartment_type = random_variables[random.randrange(len(random_variables))]
		description_template  = "\n"+"%s %s bedroom apartment with big queen sized + bedrooms, a large kitchen/common area, and big closets. Located on a prime block in %s"  %(apartment_type, str(data["beds"]),data["rental_unit"])  
		description_template += "\n"+'Live better. Call Alex today to set up your showing: 917-242-2653'
		description = description_template
		#04/10/2018
		available_date = showtime = strftime("%m/%d/%Y", gmtime())
		
		building_amenities = data["building_amenities"]
		all_amenities = building_amenities
		all_amenities = all_amenities.replace(' ',',').lower()
		amenities_array = re.split(',',all_amenities)
		checkboxArray = amenities_array
		#print(checkboxArray)
		
		
		#listing_amenity = browser.find_element_by_xpath('//*[@class="control-group check_boxes optional listing_amenity_ids rowLine"]/input[@type="checkbox"]').click()
		#print(listing_amenity)
		#sys.exit(1)
		form = {
			'address': address,
			'zipcode': zipcode,
			'location_unit_number': location_unit_number,
			'listing_type': 'Apartment',
			'beds': str(data["studio"]) if data["studio"] == "1" else str(int(data["beds"]) + int(2)),
			'baths': format(int(data["baths"]), '.1f'),
			'square_feet': data["sq_ft"] if data["sq_ft"] != "0" else "1",
			'rent': data["price"],
			'deposit': '0',
			'available_date': available_date,
			'lease_length': '4',
			'pets': '1',
			'photos': photosArray,
			'photo_descriptions': [
				"First uploaded file, pretty admirable place",
			],
			'contact': {
				'first_name': CONTACT_FIRST_NAME,
				'last_name': CONTACT_LAST_NAME,
				'email': CONTACT_EMAIL,
				'phone': CONTACT_PHONE_NUMBER,
				'contact_preference': 'Phone & Email',
				'hide_my_name': "false"
			},
			'description': description,
			'amenities': ['1', '16', '2', '15', '5', '6'],
			'laundry_type': 'Laundry Facilities',
			'parking_type': 'Covered',
			'parking_fee': '0',
		}
		
		print("Automating form entry...")
                
		#Select beds (radio)
		beds = form['beds']
		display_notice('form', 'Setting Beds: '+str(data["beds"]))
		browser.find_element_by_xpath("//input[@name='listing[apartment_size_id]' and @value='"+beds+"']").click();
		
		
		# Uploading photos
		file_input_xpath_selector = "//input[@id='image_file_upload']"
		i = 0
		for photo in form['photos']:
			if(i < 2):	
				folder = os.getcwd()+"/"
				photo_path = folder + photo
				#print(photo_path)
				display_notice('form', 'Uploading photo: '+ photo)
				form_upload_file(browser, file_input_xpath_selector, photo_path)
				time.sleep(2)
				i+=1
		#sys.exit(1)
		#Select bathrooms (radio)
		bathrooms = str(form['baths'])
		display_notice('form', 'Setting Bathrooms: '+bathrooms)
		browser.find_element_by_xpath("//input[@name='listing[bathrooms]' and @value='"+bathrooms+"']").click();
		
		#Enter rent (text)
		display_notice('form', 'Setting Rent: '+form['rent'])
		form_input_text(browser, '#listing_rent', form['rent'], True)
		
		#Enter available date (text)
		display_notice('form', 'Setting Available Date: '+form['available_date'])
		form_input_text(browser, '#listing_available', form['available_date'], True)
		
		#Enter lease length (radio)
		lease_length = str(form['lease_length'])
		display_notice('form', 'Setting Lease Length: '+form['lease_length'])
		browser.find_element_by_xpath("//input[@name='listing[lease_term]' and @value='"+lease_length+"']").click();
		
		time.sleep(2)
		#Enter square feets (text)
		display_notice('form', 'Setting Square Feet: '+form['square_feet'])
		form_input_text(browser, '#listing_square_ft', form['square_feet'], True)
		
		#Enter lease length (radio)
		pets = str(form['pets'])
		display_notice('form', 'Setting Pets: '+form['pets'])
		browser.find_element_by_xpath("//input[@name='listing[pet_id]' and @value='"+pets+"']").click();
		
		#Select Borough (select)
		display_notice('form', 'Setting Neighborhood: Manhattan')
		try:
			Borough = Select(browser.find_element_by_xpath('//*[@id="listing_borough_id"]'))
			Borough.select_by_visible_text('Manhattan')
		except:
			print( "Borough error")
		time.sleep(4)
		#Select Neighborhood (select)
		display_notice('form', 'Setting Neighborhood: '+data["rental_unit"])
		try:
			Borough = Select(browser.find_element_by_xpath('//*[@id="listing_neighborhood_id"]'))
			Borough.select_by_visible_text(data["rental_unit"])
		except:
			try:
				Borough = Select(browser.find_element_by_xpath('//*[@id="listing_neighborhood_id"]'))
				Borough.select_by_visible_text('Upper West Side')
			except:
				print('Neighborhood error')	
		
		#Enter Street Address (text)
		display_notice('form', 'Setting Street Address: '+form['address'])
		form_input_text(browser, '#listing_street_address', form['address'], True)
		#Enter Unit (text)
		display_notice('form', 'Setting Unit: '+form['location_unit_number'])
		form_input_text(browser, '#listing_unit', form['location_unit_number'], True)
		
		#Enter ZipCode (text)
		display_notice('form', 'Setting ZipCode: '+form['zipcode'])
		form_input_text(browser, '#listing_zip', form['zipcode'], True)
		
		val = form['contact']['hide_my_name']
		if val:
			msg = "Hide address from apartments"
		else:
			msg = "Don't hide address from apartments"
		display_notice('form', msg)
		browser.find_element_by_xpath("//input[@name='listing[show_address]' and @value='"+val+"']").click();
		
		"""
			Description
		"""
		display_notice('form', 'Description: '+form['description'])
		description = browser.find_element_by_xpath("//*[@id='cke_contents_listing_description']/iframe");
		description.click();
		description.send_keys(form['description']);
		
		print("Processing amenities...")
		for amenity in form['amenities']:
			display_notice('form', amenity+': '+ "True")
			form_select_checkbox2(browser, 'listing_amenity_ids_'+str(amenity), True)
		
		print("Trying to Submit form")
		
		submit_form_button = browser.find_element_by_xpath('//*[@id="wrapper"]/div[2]/div[6]/div/button[1]')
		submit_form_button.click()
		time.sleep(15)

print("End of execution, exiting the tool...")