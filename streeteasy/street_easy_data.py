﻿# coding=utf-8
import selenium
from selenium import webdriver
from selenium.webdriver.firefox.firefox_profile import FirefoxProfile
from bs4 import BeautifulSoup
from selenium.webdriver.firefox.options import Options
import os
import sys,re
from selenium.webdriver.common.by import By
import time
import csv
import urllib
from selenium.webdriver.common.keys import Keys
from decimal import Decimal
from helper_functions import(
	login_user, open_url,
	form_select_radio, form_select_option,
	form_input_text, form_select_checkbox,
	form_upload_file, form_button_click,
	display_notice, write_chunk_to_file,
	read_as_soup, write_to_file,
	get_full_img_path
)

"""
Defining variables
"""
link = "https://streeteasy.com/building/residences-at-the-mandarin-oriental/76-b"
CSV_INPUT_FILE = "streeteasy_urls.csv"
CSV_OUTPUT_FILE = "streeteasy_data.csv"

print("Starting up the Python Automation Tool street_easy..")

"""
Setting things up with web-driver
"""
reload(sys)
sys.setdefaultencoding("utf_8")
sys.getdefaultencoding()
## Initialize Firefox browser
options = Options()
options.add_argument("--headless")
# browser = webdriver.Firefox(firefox_options=options)
browser = webdriver.Firefox()

## Maximize window and open the base url
browser.maximize_window()
try:
    os.remove(CSV_OUTPUT_FILE)
except OSError:
    pass

raw_data_file = open(CSV_INPUT_FILE, 'rU')
csv_data_file = csv.reader(raw_data_file, delimiter=',', quotechar='"')
csv_data_file.next()

write_to_file('url,address,state,placename,agent_name,agent_info,phone_number,price,sq_ft,price_per_sq_ft,rooms,beds,studio,baths,rental_unit,amenities,building_amenities,listing_amenities,images,description', CSV_OUTPUT_FILE)
## Step 1
for each in csv_data_file:
	if each:
	
		#print(each[2])
		listing_detail_url = each[2]
		#listing_detail_url = "https://streeteasy.com/building/41-park-avenue-new_york/017c?featured=1"
		# 1.1 Open listing page
		open_url(browser, listing_detail_url)
		time.sleep(10)
		
		page = browser.find_element_by_xpath("//*")
		allHTML=page.get_attribute('innerHTML')
		html=allHTML.encode('utf-8')
		index=html.find("We can't seem to select_one the page you're looking for.")

		soup = BeautifulSoup(html, 'html.parser')
		placename = soup.find("meta",  property="geo.placename")
		#placename = placename["content"]
		#print(placename)
		#sys.exit(1)
		current_page_rows = []
		#html_content = read_as_soup(browser)
		for html in soup.select('div#content'):
			# Step 2
			# Begin scraping
			listing_url = listing_detail_url
			## 2.1
			# Title
			try:
				title = html.select_one('.building-title').text
				title = title.strip()
			except:
				title = ""
			
			# Agent Name
			agent_column = html.select_one("div.ListingAgents-agentListItem span.ListingAgents-agentInfoContainer")
			try:
				agent_name = agent_column.select('span')[0].text
				agent_name = agent_name.strip()
			except:
				agent_name = ""
			
			# Agent Info
			try:
				agent_info = agent_column.select('span')[1].text
				agent_info = agent_info.strip()
			except:
				agent_info = ""
			
			# Agent phone number
			try:
				phone_number = agent_column.select_one('span.ListingAgents-hiddenPhone').text
				phone_number = phone_number.strip()
			except:
				phone_number = ""
			
			# Price
			try:
				price = html.select_one('.price').text
				price = price.replace("for rent", "").replace("&darr;", "").replace("$", "").replace(",", "").strip()
				price = price.encode('cp437', 'ignore').strip()
			except:
				price = "0"
			# get sq ft, price per sq ft, rooms, beds,baths	
			right_container = html.select_one('article.right-two-fifths')
			detail_cell = right_container.select('span.detail_cell')
			
			details=''
			for d in detail_cell:
				detail=d.text
				details=details+str(detail.strip())+":"
			details=details.replace("'","")
			details=details.strip()
			details=details[:-1]
			
			t_details = details.split(':')
			baths = "0"
			beds = "0"
			rooms = "0"
			sq_ft = "0"
			price_per_sq_ft = "0"
			studio = '0'
			size = ''
			for feature in t_details:
				if 'bed' in feature:
					#beds = str(filter(str.isdigit, feature)).strip()
					beds = re.findall(r"[-+]?\d*\.\d+|\d+", feature)
					beds = beds[0].strip()
				if 'bath' in feature:
					baths = re.findall(r"[-+]?\d*\.\d+|\d+", feature)
					baths = baths[0].strip()
					#baths = str(filter(str.isdigit, feature)).strip()
				if 'room' in feature:
					rooms = re.findall(r"[-+]?\d*\.\d+|\d+", feature)
					rooms = rooms[0].strip()
					#rooms = str(filter(str.isdigit, feature)).strip()
				if 'studio' in feature:
					studio = "1"
				if 'ft²' in feature and 'per' not in feature:
					sq_ft = feature
					sq_ft = sq_ft.replace("ft²", "").replace(",", "").strip()
					sq_ft = sq_ft.encode('cp437', 'ignore').strip()
				if 'per ft²' in feature:
					price_per_sq_ft = feature
					price_per_sq_ft = price_per_sq_ft.replace("$", "").replace("per ft²", "").strip()
					price_per_sq_ft = price_per_sq_ft.encode('cp437', 'ignore').strip()
					
			#print "Beds: "+str(beds)
			#print "Baths: "+str(baths)
			#print "rooms: "+str(rooms)
			#print "sq_ft: "+str(sq_ft)
			#print "price_per_sq_ft: "+str(price_per_sq_ft)
			#sys.exit(1)
			
			# Rental Unit
			try:
				rental_unit = html.select_one('div.details_info a').text
				rental_unit = rental_unit.strip()
			except:
				rental_unit = ""
				
			"""
			
			# Details
			try:
				details_info = html.select_one(".details_info")
				sq_ft = details_info.select('span')[0].text
				sq_ft = sq_ft.replace("ft²", "").replace(",", "").strip()
				sq_ft = sq_ft.encode('cp437', 'ignore').strip()
			except:
				sq_ft = "0"
			# Price per sq ft
			try:
				price_per_sq_ft = details_info.select('span')[1].text
				price_per_sq_ft = price_per_sq_ft.replace("$", "").replace("per ft²", "").strip()
				price_per_sq_ft = price_per_sq_ft.encode('cp437', 'ignore').strip()
			except:
				price_per_sq_ft = "0"
			# Rooms
			try:
				rooms = details_info.select('span')[2].text
				rooms = rooms.replace("rooms", "").strip()
				rooms = rooms.encode('cp437', 'ignore').strip()
			except:
				rooms = "0"
			#beds
			try:
				beds = details_info.select('span')[3].text
				beds = beds.replace("beds", "").strip()
				beds = beds.encode('cp437', 'ignore').strip()
			except:
				beds = "0"
			# Baths
			try:
				baths = details_info.select('span')[4].text
				baths = baths.replace("baths", "").strip()
				baths = baths.encode('cp437', 'ignore').strip()
			except:
				baths = "0"
			"""
			# Description
			try:
				desc = html.select_one("section.DetailsPage-contentBlock div.Description-block p")
				desc = desc.text.strip()
				desc = desc.encode('cp437', 'ignore').strip()
			except:
				desc = ""
			#State
			try:
				state = html.select_one("div.BuildingInfo-item span")
				state = state.text.strip()
				state = state.encode('cp437', 'ignore').strip()
				if state == 'This Building:':
					#state = html.select_one("div.BuildingInfo-item a")
					#state = state.text.strip()
					#state = state.encode('cp437', 'ignore').strip()
					state = "New York, NY"
			except:
				state = ""

			# Amenities
			try:
				amenities_scraped = []
				amenities_block = html.select("ul.AmenitiesBlock-highlights li")
				for amenity in amenities_block:
					amenities_scraped.append(amenity.text.strip())
				amenities_scraped = ",".join(amenities_scraped)
			except:
				amenities_scraped = ""
			
			# Building amenities
			try:
				building_amenities_scraped = []
				building_amenities_block = html.select("ul.AmenitiesBlock-list li")
				for amenity in building_amenities_block:
					if "googletag" not in amenity.text.strip(): 
						building_amenities_scraped.append(amenity.text.strip())
				building_amenities_scraped = ",".join(building_amenities_scraped)
			except:
				building_amenities_scraped = ""
			# Listing Amenities
			try:
				listing_amenities_scraped = []
				listing_amenities_block = html.select("ul.AmenitiesBlock-list li")
				for amenity in listing_amenities_block:
					if "googletag" not in amenity.text.strip():
						listing_amenities_scraped.append(amenity.text.strip())
				listing_amenities_scraped = ",".join(listing_amenities_scraped)
			except:
				listing_amenities_scraped= ""

			# Images
			img_urls_scraped = []
			img_urls = html.select('ul.lSPager.lSGallery li')
			for url in img_urls:
				img_url = url.select_one('a img')['src']
				print("Photo = "+img_url)
				img_url=img_url.replace("https","http")
				directory = "images"
				if not os.path.exists(directory):
					os.makedirs(directory)
				filename = img_url.split("/")[-1] 
				image_path=directory+"/"+filename
				try:
					urllib.urlretrieve(img_url,image_path)
				except:
					print "Image could not download"
				
				img_urls_scraped.append(image_path)
			
			img_urls_scraped = ",".join(img_urls_scraped)
			time.sleep(10)
			#print("title:"+ title+" & price:"+ price + sq_ft + price_per_sq_ft + rooms + beds + baths + desc )
			print("Address:" + title)
			print("state:" + state)
			print("placename:" + str(placename))
			print "agent_name: "+str(agent_name)
			print "agent_info: "+str(agent_info)
			print "phone_number: "+str(phone_number)
			print("price:" + price)
			print("sq_ft:" + sq_ft)
			print("price_per_sq_ft:" + price_per_sq_ft)
			print("rooms:" + str(rooms))
			print("beds:" + str(beds))
			print("studio:" + str(studio))
			print("baths:" + str(baths))
			print("rental_unit:" + str(rental_unit))
			print("desc:" + desc)
			print("amenities_scraped:" + amenities_scraped)
			print("building_amenities_scraped:" + building_amenities_scraped)
			print("listing_amenities_scraped:" + listing_amenities_scraped)
			print("img_urls_scraped:" + img_urls_scraped)
			current_page_rows.append([str(listing_url),str(title),state,str(placename),str(agent_name),str(agent_info),str(phone_number),price,sq_ft,price_per_sq_ft,str(rooms),str(beds),str(studio),str(baths),str(rental_unit),amenities_scraped,building_amenities_scraped,listing_amenities_scraped,img_urls_scraped,desc])
		write_chunk_to_file(current_page_rows, CSV_OUTPUT_FILE)
		print "#########################################################################################################"
		#sys.exit(1)
browser.close()
print("End of script")