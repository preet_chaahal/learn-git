import re
import time
import os
import sys
import csv
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.firefox.firefox_profile import FirefoxProfile

firefox_capabilities = DesiredCapabilities.FIREFOX
firefox_capabilities['marionette'] = True
browser = webdriver.Firefox(capabilities=firefox_capabilities)
CSV_URLS_FILE = "streeteasy_urls.csv"
print "Starting Python automation script for get links from streeteasy........"
browser.get("https://streeteasy.com/for-rent/nyc/price:1700-75000%7Carea:102,119,139,135?refined_search=true")
time.sleep(3)
def write_to_file(data, filename):
	print("Generating CSV data")
	pin = open(filename,"a+")
	pin.write(data+"\n")
	pin.close()

def write_chunk_to_file(data_set, filename):
	with open(filename, 'a+') as f:
		writer = csv.writer(f)
		writer.writerows(data_set)
	print("Write successful!")
	return True
def extract_data(browser):
    links = browser.find_elements_by_xpath('//*[@class="details-title"]/a')
    return [link.get_attribute('href') for link in links]

# get max pages

element = WebDriverWait(browser, 10).until(EC.presence_of_element_located((By.XPATH, '//*[@id="result-details"]/main/div[3]/nav/span[@class="page"][last()]')))
max_pages = int(element.text)
print("Total pages:" + str(max_pages))

write_to_file('sno,page,url', CSV_URLS_FILE)

# extract from the current (1) page
print "Page 1"
try:
	links = extract_data(browser)
	current_page_rows1 = []
	link_counter1 = 1
	page = 1
	for link in links:
		print (link)
		current_page_rows1.append([str(link_counter1),page,link])
		link_counter1 += 1
	write_chunk_to_file(current_page_rows1, CSV_URLS_FILE)
except:
	print("Page 1 urls scrape failed");	

time.sleep(2)
# loop over the rest of the pages
link_counter = 1
for page in xrange(2, max_pages+1):
    print "Page %d" % page

    next_page = browser.find_element_by_xpath("//*[@id='result-details']/main/div[3]/nav/span[@class='next']/a").click()
    links = extract_data(browser)
    current_page_rows = []
    for link in links:
        print (link)
        current_page_rows.append([str(link_counter),page,link])
        link_counter += 1
    write_chunk_to_file(current_page_rows, CSV_URLS_FILE)
    time.sleep(2)
    print "----------------------------------------------------------"

browser.close()
print("End of script")
