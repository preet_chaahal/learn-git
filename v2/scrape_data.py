import selenium
from selenium import webdriver
from selenium.webdriver.firefox.firefox_profile import FirefoxProfile
from bs4 import BeautifulSoup
from selenium.webdriver.firefox.options import Options
import os
import sys
from selenium.webdriver.common.by import By
import time
from selenium.webdriver.common.keys import Keys
from helper_functions import(
	login_user, open_url,
	form_select_radio, form_select_option,
	form_input_text, form_select_checkbox,
	form_upload_file, form_button_click,
	display_notice, write_chunk_to_file,
	read_as_soup, write_to_file,
	get_full_img_path
)

"""
Defining variables
"""
link = "https://streeteasy.com/building/residences-at-the-mandarin-oriental/76-b"
CSV_OUTPUT_FILE = "data.csv"

print("Starting up the Python Automation Tool (street_easy_v2)..")

"""
Setting things up with web-driver
"""
## Initialize Firefox browser
options = Options()
options.add_argument("--headless")
# browser = webdriver.Firefox(firefox_options=options)
browser = webdriver.Firefox()

## Maximize window and open the base url
browser.maximize_window()

## Step 1
# 1.1 Open listing page
open_url(browser, link)
time.sleep(15)

html = read_as_soup(browser)
# Step 2
# Begin scraping

## 2.1
# Title
title = html.select_one('.building-title').text
title = title.strip()

# Price
price = html.select_one('.price').text
price = price.replace("for rent", "").replace("↓", "").replace("$", "").replace(",", "").strip()

# Details
details_info = html.select_one(".details_info")
sq_ft = details_info.select('span')[0].text
sq_ft = sq_ft.replace("ft²", "").replace(",", "").strip()

price_per_sq_ft = details_info.select('span')[1].text
price_per_sq_ft = price_per_sq_ft.replace("$", "").replace("per ft²", "").strip()

rooms = details_info.select('span')[2].text
rooms = rooms.replace("rooms", "").strip()

beds = details_info.select('span')[3].text
beds = beds.replace("beds", "").strip()

baths = details_info.select('span')[4].text
baths = baths.replace("baths", "").strip()

desc = html.select_one("section.DetailsPage-contentBlock div.Description-block p")
desc = desc.text.strip()

# Amenities
amenities_scraped = []
amenities_block = html.select("ul.AmenitiesBlock-highlights li")
for amenity in amenities_block:
	amenities_scraped.append(amenity.text.strip())

# Building amenities
building_amenities_scraped = []
building_amenities_block = html.select("ul.AmenitiesBlock-list li")
for amenity in building_amenities_block:
	building_amenities_scraped.append(amenity.text.strip())

# Listing Amenities
listing_amenities_scraped = []
listing_amenities_block = html.select("ul.AmenitiesBlock-list li")
for amenity in listing_amenities_block:
	listing_amenities_scraped.append(amenity.text.strip())


# Images
img_urls_scraped = []
img_urls = html.select('ul.lSPager.lSGallery li')
for url in img_urls:
	thumb = url.select_one('a img')['src']
	print("thumb=="+thumb)
	full = get_full_img_path(thumb)
	print("Full img")
	print(full)
	# imgs = {'thumb': thumb, 'full': get_full_img_path(thumb)}
	# img_urls_scraped.append(imgs)


print("title:"+ title+" & price:"+ price + sq_ft + price_per_sq_ft + rooms + beds + baths + desc )
print(amenities_scraped)
print(building_amenities_scraped)
print(listing_amenities_scraped)
print(img_urls_scraped)
print("End of script!")