import selenium
from selenium import webdriver
from selenium.webdriver.firefox.firefox_profile import FirefoxProfile
from bs4 import BeautifulSoup
from selenium.webdriver.firefox.options import Options
import os
import sys
from selenium.webdriver.common.by import By
import time
from selenium.webdriver.common.keys import Keys
from helper_functions import(
	login_user, open_url,
	form_select_radio, form_select_option,
	form_input_text, form_select_checkbox,
	form_upload_file, form_button_click,
	display_notice, write_chunk_to_file,
	write_to_file
)

"""
Defining variables
"""
URL_SEARCH_PAGE = "https://streeteasy.com/for-rent/nyc/price:1-100000%7Carea:102,119,139,135"

CSV_LINKS_FILE = "links.csv"

# https://streeteasy.com/for-rent/nyc/price:1-100000%7Carea:102,119,139,135?page=688

print("Starting up the Python Automation Tool (street_easy_v2)..")

"""
Setting things up with web-driver
"""
## Initialize Firefox browser
options = Options()
options.add_argument("--headless")
browser = webdriver.Firefox(firefox_options=options)

## Maximize window and open the base url
browser.maximize_window()

## Step 1
# 1.1 Open listing page
open_url(browser, URL_SEARCH_PAGE)
time.sleep(3)

print("Reading total pages to scrape..")
total_pages = browser.find_element(By.XPATH, "//div[@class='pagination center_aligned bottom_pagination big_space']/nav[@class='pagination']/span[@class='page'][3]/a").text

print("Total pages:"+ total_pages)

write_to_file('sno,title,link', CSV_LINKS_FILE)

link_counter = 1
for i in range(1,int(total_pages)+1):
	open_url(browser, URL_SEARCH_PAGE+"?page="+ str(i))
	## Read page and process data
	page = browser.find_element_by_xpath("//*")
	allHTML=page.get_attribute('innerHTML')
	html=allHTML.encode('utf-8')
	soup = BeautifulSoup(html, 'html.parser')

	current_page_rows = []
	for div in soup.select('article.item'):
		head=div.select_one('h3.details-title')
		title = head.select_one('a').text.strip()
		link = "https://streeteasy.com"+head.select_one('a').get('href')
		current_page_rows.append([str(link_counter),title,link])
		link_counter += 1
	print("Writing "+ str(len(current_page_rows)) +" scraped links")
	write_chunk_to_file(current_page_rows, CSV_LINKS_FILE)
    
print("End of script!")